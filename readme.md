# instructions for building the app

## platforms:
Every platform basically requires your app to be built on that platform.
Because of platform-specific libraries that won't run on the other OSes
So an osx app has to be built on osx, windows apps on windows.

## preparation
In `main.js` comment out/remove the `electron-reload` (line 8) line and in the app ready function, comment out/remove the lines that make the webinspector auto open up.

## build command
just run
* `npm run packageOSX` for osx builds
* `npm run packageWin` for windows builds