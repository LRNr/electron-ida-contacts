const angular = require('angular')
const { ipcRenderer } = require('electron')

function Controller ($scope, $location) 
{
	var ctrl = this;
	ctrl.findPeeps = findPeeps
	ctrl.goto = gotoPerson

	// init with all contacts
	ipcRenderer.send('findPeeps', {type: 'name', value: {$exists : true}})

	/* ---------------------------------- */

	function findPeeps (name) {
		if (name && name.length < 2) return;
		ctrl.loading = true;
		let query = name !== '' ? {type:'name', value: name} : {}
		ipcRenderer.send('findPeeps', query)
	}

	function gotoPerson (person) {
		$location.path(`/contact/${person._id}`)
	}

	ipcRenderer.on('foundPeeps', (ev, resp) => {
		if (resp.err) ctrl.error = resp.err;
		else  if (resp.contacts.length > 0) {
			ctrl.contacts = resp.contacts
		}
		else {
			ctrl.contacts =  []
			ctrl.error = 'These are not the contacs you\'re looking for'
		}
		ctrl.loading = false;
		$scope.$digest()
	})

	ipcRenderer.on('error', (ev, msg) => {
		ctrl.error = msg
		$scope.$digest();
	})

	$scope.$on('$destroy', () => {
		ipcRenderer.removeAllListeners('foundPeeps')
		ipcRenderer.removeAllListeners('error')
	})
}

Controller.$inject = ['$scope', '$location'];

angular.module('iDA.contacts.startPage', []).component('startPage', 
{
	templateUrl : 'ui/components/start-page/start-page.html',
	controller : Controller
})

module.exports = 'iDA.contacts.startPage'