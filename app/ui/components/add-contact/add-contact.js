const angular = require('angular')
const { ipcRenderer } = require('electron')
const contactForm = require('../contact-form/contact-form.js')
const name = 'iDA.contacts.addContact'


function Controller ($scope, $location) 
{
	var ctrl = this;

	ctrl.createContact = (contact, form) => {
		ipcRenderer.send('add', contact)
		ctrl.loading = true;
		ctrl.form = form;
	}

	ipcRenderer.on('addDone', (ev, resp) => {
		ctrl.loading = false
		if (resp.err) ctrl.message = { type: 'error', content : err }
		else { 
			ctrl.message = { type : 'info', content : 'Contact created succesfully' }
			ctrl.contact = {}
			if (ctrl.form) ctrl.form.$setPristine()
		}
		$scope.$digest();
	})
}

Controller.$inject = ['$scope', '$location'];

angular.module(name, [contactForm]).component('addContact', 
{
	templateUrl : 'ui/components/add-contact/add-contact.html',
	controller : Controller
})

module.exports = name