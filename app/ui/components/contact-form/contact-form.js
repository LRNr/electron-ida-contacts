const angular = require('angular')
const name = 'iDA.contacts.contactForm'

function Controller ($scope) 
{
	let ctrl = this;
	ctrl.extraFields = []
	ctrl.submit = submit
	ctrl.$onInit = onInit
	ctrl.addField = addField

	function submit (contact, form) {
		ctrl.onUpdate({contact: contact, form:form})
	}
	function onInit () {
		if (ctrl.contact) ctrl.contact.phoneNumber = ctrl.contact.phoneNumber.replace(/\s/g,'')
	}
	function addField(fieldName) {
		console.log('add field?')
		let len = ctrl.extraFields.length + 1
		ctrl.extraFields.push({
			name: 'field' + len,
			value: null
		})
	}
}

Controller.$inject = ['$scope'];

angular.module(name, []).component('contactForm', 
{
	templateUrl : 'ui/components/contact-form/contact-form.html',
	controller : Controller,
	bindings : {
		contact : '<',
		submitText: '@',
		onUpdate : '&'
	}
})

module.exports = name