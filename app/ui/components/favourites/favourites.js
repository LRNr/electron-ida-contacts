const angular = require('angular')
const { ipcRenderer } = require('electron')
const NAME = 'favourites'

function Controller ($scope, $location, $timeout) 
{
	var ctrl = this;
	ctrl.goto = gotoPerson

	// init with all contacts
	ipcRenderer.send('findPeeps', {type: 'favourite', value:true})

	/* ---------------------------------- */

	function gotoPerson (person) {
		$location.path(`/contact/${person._id}`)
	}

	function updateDoneHandler (ev, resp)
	{
		if (resp.err) ctrl.error = resp.err;
		else ipcRenderer.send('findPeeps', {type: 'favourite', value:true})		
	}

	function foundPeepsHandler (ev, resp)
	{
		console.log(resp)
		if (resp.err) { ctrl.error = resp.err; console.error('ERROR! ', resp.err) }
		else if (resp.contacts.length >=1) {
			ctrl.contacts = resp.contacts
		}
		else {
			ctrl.contacts = [] 
			ctrl.error = "Nothing to see here; Going back"
			$timeout(() => {
				ctrl.error = null;
				$location.path('/')
			}, 1000)
		}
		ctrl.loading = false;
		$scope.$digest()
	}

	ipcRenderer.on('foundPeeps', foundPeepsHandler)

	ipcRenderer.on('updateDone', updateDoneHandler)

	$scope.$on('$destroy', () => {
		ipcRenderer.removeAllListeners('foundPeeps')
		ipcRenderer.removeAllListeners('error')
		ipcRenderer.removeListener('updateDone', updateDoneHandler)
	})
}

Controller.$inject = ['$scope', '$location', '$timeout'];

angular.module('iDA.contacts'+NAME, []).component(NAME, 
{
	templateUrl : 'ui/components/favourites/favourites.html',
	controller : Controller
})

module.exports = 'iDA.contacts'+NAME