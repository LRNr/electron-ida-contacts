const angular = require('angular')
const name = 'iDA.contacts.contact'
const { ipcRenderer } 	= require('electron')

function Controller ($scope) {
	var ctrl = this;

	function addListener () 
	{
		ipcRenderer.once('updateDone', (ev, resp) => {
			if (resp.err) ctrl.person.favourite = !ctrl.person.favourite;
			$scope.$digest()
		})
	}

	ctrl.toggleFave = (event, person) =>
	{
		event.stopPropagation()
		ipcRenderer.send('update', person._id, {favourite: person.favourite === undefined ? true : !person.favourite})
		addListener()
	}
}

Controller.$inject = ['$scope'];

angular.module(name, []).component('contact', 
{
	templateUrl : 'ui/components/contact/contact.html',
	controller : Controller,
	bindings : {
		person : '<',
		hasLink : '@',
		canFave : '<'
	}
})

module.exports = name