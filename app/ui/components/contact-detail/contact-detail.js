const angular 			= require('angular')
const { ipcRenderer } 	= require('electron')
const contactForm = require('../contact-form/contact-form.js')
const name 				= 'iDA.contacts.contactDetail'

function Controller ($scope, $routeParams, $timeout, $location) {
	let ctrl = this;
	ctrl.original = null;
	ctrl.editMode = false;
	
	ctrl.edit = (contact, editMode) => {
		ctrl.editMode = !editMode;
		if (ctrl.editMode) ctrl.original = angular.copy(contact)
		else { 
			ctrl.contact = angular.copy(ctrl.original)
			ctrl.message = null
		}
	}

	ctrl.delete = (contact) => {
		ctrl.loading = true;
		ipcRenderer.send('delete', contact._id)
	}

	ctrl.save = (contact, form) => {
		ctrl.loading = true
		ipcRenderer.send('update', contact._id, contact)
	}

	ipcRenderer.on('deleteDone', (ev, resp) => {
		ctrl.loading = false
		ctrl.deleted = true
		if (resp.err) ctrl.message = {type : 'error', content : err}
		else {
			ctrl.message = {type:'info', content : 'Contact deleted'}
			$timeout(()=>{
				$location.path('/')
			}, 1000)
		}
		$scope.$digest()
	})

	ipcRenderer.on('updateDone', (ev, resp) => {
		ctrl.loading = false
		if (resp.err) {ctrl.message = {type:'error', content:resp.err}; console.log('update error', resp) }
		else {
			ctrl.message = {type:'info', content : 'Update successful'}
			ctrl.edit(ctrl.contact, ctrl.editMode)
			ipcRenderer.send('findPeeps', {type:'_id', value:ctrl.contact._id})
		}
		$scope.$digest()
	})

	ipcRenderer.on('foundPeeps', (ev, resp) => {
		ctrl.loading = false
		ctrl.editMode = false
		if (resp.err) ctrl.message = {type:'error', content:resp.err}
		else {
			ctrl.contact = resp.contacts[0]
		}
		ctrl.loading = false;
		$scope.$digest()
	})

	ctrl.loading = true;
	ipcRenderer.send('findPeeps', {type: '_id', value: $routeParams.id})

	$scope.$on('$destroy', () => {
		ipcRenderer.removeAllListeners('deleteDone')
		ipcRenderer.removeAllListeners('foundPeeps')
		ipcRenderer.removeAllListeners('updateDone')
	})
}


Controller.$inject = ['$scope', '$routeParams', '$timeout', '$location'];

angular.module(name, [contactForm]).component('contactDetail', 
{
	templateUrl : 'ui/components/contact-detail/contact-detail.html',
	controller : Controller
})

module.exports = name