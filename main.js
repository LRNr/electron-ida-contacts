'use strict';

const { app, BrowserWindow, ipcMain } = require('electron')
const DataStore = require('nedb')
const fse = require('fs-extra')
const del = require('del')
const Q = require('q')
require('electron-reload')(`${__dirname}/**/*`); // auto-reload

let contacts, fsDB, appDB
let mainWindow
let error = ''
const dbPath = '/Team Awesome/Contacts.db'
const dbAssetPath = `${__dirname}/app/assets/Contacts.db`
const dbTargetPath = `${app.getPath('appData')}${dbPath}`
const logFile = `${app.getPath('appData')}/Team Awesome/error.log`

/**
*	check if there is already a db file on the users filesystem
* 	if it doesn't copy the internal one
* 	if there is, activate it, compact it, check the app version
**/
if (!fse.existsSync(dbTargetPath)) copyDB();
else {
	contacts = new DataStore({filename : dbTargetPath, autoload:true});
	contacts.persistence.compactDatafile
	checkAppDbVersion()
}	

/*
*	put error messages in log file
**/
function logError(message)
{
	return fse.appendFile(logFile, `\n [${new Date().toISOString()}] ${message}` )
}


/*
*	copy the app's internal database to the filesystem
*	and execute an optional callback fn
* 	send error to view if fail
*/

function copyDB(fn)
{
	fse.copy(dbAssetPath, dbTargetPath, (err) => {
		if (err) error += err + '\ncurrent directory: ' + __dirname
		else {
			contacts = new DataStore({filename : dbTargetPath, autoload:true})
			if (fn) fn()
		}
	})
}

/*
*	adds an entry with the app version to the database
*/
function addAppVersionToDB()
{
	contacts.insert({appVersion : app.getVersion()}, (err, num) => {
		if (err) return logError('FAILED SETTING APP VERSION IN DB')
	})
}

/*
*	Create a list of entries that should be marged
* 	into the new database when it gets replaced
*/
function createMergeList (oldDocs, newDocs)
{
	return oldDocs.filter(oldItm => {
		let test = newDocs.find(newItm => newItm.name === oldItm.name)
		return test === undefined
	}).map(doc => {
		delete doc._id
		return doc
	})
}

/*
*	Create a list of entries that have been given extra fields
* 	so we can keep those fields when replacing the db
*/
function createUpdateList (oldDocs, newDocs)
{
	return oldDocs.filter(oldItm => {
		let test = newDocs.find(newItm => newItm.name === oldItm.name && Object.keys(newItm).length !== Object.keys(oldItm).length)
		return test !== undefined
	}).map(doc => {
		// TODO only keep properties that are not in the newDocs vesion
		let mapDoc = {
			name : doc.name
		}, 
		newDoc = newDocs.find(newItem => newItem.name === doc.name)
		for (var prop in doc) {
			if (doc.hasOwnProperty(prop) && !newDoc.hasOwnProperty(prop)) {
				mapDoc[prop] = doc[prop]
			}
		}
		return mapDoc
	})
}

/*
*	reject a promise and log an error message to the logfile
*/
function rejectAndLog (deferred, message) {
	deferred.reject('fail')
	return logError(message)
}

/*
*	Merge 2 database files when the app contains a newer one than the filesystem
*	gathers all the content of both databases and goes over the entries
*	to find extra entries and entries with extra props to merge those
*/
function mergeDBs ()
{
	let userDB = new DataStore({filename: dbTargetPath, autoload: true})
	let appDB = new DataStore({filename : dbAssetPath, autoload:true})
	let userContacts, appContacts
	let deferred = Q.defer()

	userDB.find({}, (err, docs) => {
		if (err)  return rejectAndLog(deferred, `FAILED getting user db data : ${err}`)

		userContacts = docs
		appDB.find({}, (err, docs) => {
			if (err) return rejectAndLog(deferred, `FAILED getting app db data: ${err}`)

				appContacts = docs
			let updateList = createUpdateList(userContacts, appContacts)
			let mergeList = createMergeList(userContacts, appContacts)
			let paths = del.sync(dbTargetPath, {force: true})
			
			if (!paths) return rejectAndLog(deferred, `FAILED deleting user db before merge`)

			copyDB(() => {
				if (mergeList.length > 0) {
					contacts.insert(mergeList, (err, docs) => {
						if (err) return rejectAndLog(deferred, `FAILED inserting docs to merge: ${err}`)
					})
				}
				if (updateList.length > 0) updateList.forEach(doc => {
					let name = doc.name
					delete doc.name
					contacts.update({name : name}, {$set : doc}, (err, num) => {
						if (err) logError(`FAILED to update record for ${name}: ${err}`)
					})
				})
			})
		})
	})

	return deferred.promise
}

/*
*	check the version of the database on the user's file system
*/
function checkAppDbVersion() 
{
	contacts.find({appVersion: {$exists : true}}, (err, res) => {
		// if there is no app version or an old app version in the db > replace it 
		if (res.length === 0 || res[0].appVersion !== app.getVersion()) {
			mergeDBs().then(()=>{
				addAppVersionToDB()
			})
		}
	})
}


app.on('ready', () => {
	mainWindow = new BrowserWindow({
		frame : false,
		width : 450,
		useContentSize: true,
		icon : './app/assets/app-icon.png'
	})
	mainWindow.loadURL(`file://${__dirname}/app/index.html`)
	if (error) {
		mainWindow.webContents.on('did-finish-load', () => {
			mainWindow.webContents.send('error', error)
		})
	}
	mainWindow.webContents.openDevTools()
})


/*
*	Find contacts with any part of their name string matching the query, uses regex
*	returns ipc braodcast with error message and any results found
* 	@params {string} name 		name to search for
*/
ipcMain.on('findPeeps', (ev, query) => {
	let search = {}
	if (query.hasOwnProperty('type') && query.hasOwnProperty('value')) search[query.type] = query.type === 'name' ? new RegExp(query.value, 'i') : query.value

		contacts.find(search).sort({name: 1}).exec((err, res) => {
			return ev.sender.send('foundPeeps', {err: err, contacts: res});
		});
});


/*
*	delete a contact from the database using their id
*	returns an ipc broadcast with any error message and the number of deleted contacts
*	@params {string} id 	contact id
**/
ipcMain.on('delete', (ev, id) => {
	contacts.remove({ _id : id}, (err, num) => {
		return ev.sender.send('deleteDone', {err: err, num: num})
	})
})


/*
*	update a contact from the database using their id
*	returns an ipc broadcast with any error message and the number of updated contacts
*	@params {string} id 		contact id
*	@params {object} newData	new data to set
**/
ipcMain.on('update', (ev, id, newData) => {
	delete newData.$$hashKey
	contacts.update({ _id : id}, { $set: newData }, (err, num) => {
		return ev.sender.send('updateDone', {err: err, num: num})
	})
})

/*
*	add a contact to the database
*	returns an ipc broadcast with any error message and the number of added contacts
*	@params {object} contact	data of new contact to add
**/
ipcMain.on('add', (ev, contact) => {
	contacts.insert(contact, (err, num) => {
		return ev.sender.send('addDone', {err: err, num: num})
	})
})

