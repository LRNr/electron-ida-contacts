const angular = require('angular')
const ngRoute = require('angular-route')
const ngAnimate = require('angular-animate')
const startPage = require('./components/start-page/start-page.js')
const favourites = require('./components/favourites/favourites.js')
const contactDetail = require('./components/contact-detail/contact-detail.js')
const contact = require('./components/contact/contact.js')
const addContact = require('./components/add-contact/add-contact.js')

var app = angular.module('iDA.contacts', [
	ngAnimate,
	ngRoute,
	startPage,
	favourites,
	contactDetail,
	contact,
	addContact
]).config(['$routeProvider', '$locationProvider', '$compileProvider', function($routeProvider, $locationProvider, $compileProvider) 
{
	$routeProvider
	.when('/', {
		template : '<start-page></start-page>'
	})
	.when('/favourites', {
		template : '<favourites></favourites>'
	})
	.when('/add-contact', {
		template : '<add-contact></add-contact>'
	})
	.when('/contact/:id', {
		template : '<contact-detail></contact-detail>'
	})

	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|skype):/);
	// $locationProvider.html5Mode(true)
}]).filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
}).run(['$rootScope','$location', '$http', function($rootScope, $location, $http) 
{

}])